# Satsui

```
Satsui started as a project based around self-driven sustainability, action, and responsibility. This project is my attempt to apply these principles not only in my own practice, but also to create modular microservices that contain their own actions, responsibility, and limitations, to allow a self-driven, self-assemblying, and self-scaling system
```

## Projects

| Project | Version | Purpose |
| ------- | ------- | ------- |

## Technologies

[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/)